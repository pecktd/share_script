import maya.cmds as mc

def copy_weight(from_this, to_this):
    infs = mc.skinCluster(from_this, q=True, inf=True)
    mc.skinCluster(infs, to_this, tsb=True)
    mc.select(from_this, to_this, r=True)
    mc.copySkinWeights(
        noMirror=True,
        surfaceAssociation="closestPoint",
        influenceAssociation="closestJoint"
    )

# copy_weight("old_geo", "new_geo")
